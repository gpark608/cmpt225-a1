/*
 * WalkIn.cpp
 *
 * Description: Driver/client code. Gives the users options to:
 *              Add patients
 *              Remove patients & Remove All patients
 *              Modify patients (name; address, email, and phone)
 *              Search for patients
 *              Print patient list
 *  
 * Author: Michael Park(#301360758), Praneeth Ellanti (#301381164)
 * Date:Jan 22, 2019
 */


#include <iostream>
#include <string>
#include "List.h"
#include "Patient.h"

using namespace std;

//Description: Allows the client to add patients to the list
//Precondition: The carecard length has to be 10 (else it exits)
//              The list must not be at full capacity 
//              The carecard must not be a duplicate
//				Assumes all the input is well-formed (string of integer)
//Postcondition: The carecard is added to list
void add(List& thePatients)
{
	string theCareCard = "";
	cout << "Please, enter Patient's CareCard Number: ";
	getline(cin >> ws, theCareCard);
	Patient Patient(theCareCard);
	


    	if (thePatients.insert(theCareCard) == true)
    	{
    		if (theCareCard.length() == 10)
    		{
    			cout << "CareCard " << theCareCard << " has been added to list\n "<< endl;
    			thePatients.printList();
    		}
    		else if (theCareCard.length()!=10)
    		{
    		cout << "CareCard Number entered is not Valid. It has been set to 0000000000 as default "<< endl;
    		thePatients.printList();
    		}
    	}
    	else 
    	{
    		cout << "Adding patient was unsuccessful" << endl;
    		thePatients.printList();
    	}
}
	
	


//Description: Allows the client to remove patients to the list
//Precondition: There must at least be 1 element in the list
//              The carecard to be removed must be in the list
//Postcondition: The carecard is removed from the list and the list is printed
void remove(List& thePatients) {
	string theCareCard = "";
	if (thePatients.getElementCount() > 0)
	{
		cout << "Please, enter the CareCard Number of the Patient leaving the clinic: ";
		cin >> theCareCard;
		Patient Patient(theCareCard);
		if (thePatients.remove(theCareCard) == true)
		{
			cout << "Patient Profile " << theCareCard << " has been removed\n" << endl;
		}
		else
		{
			cout << "Patient Profile " << theCareCard << " does not exist\n" << endl;
		}
	}
	else
	{
		cout<<"No Patient is registered yet"<< endl;
	}
	thePatients.printList();
}

//Description: Allows the client to seek the patients from the list
//Precondition: There must be at least one patient in the list
//              The carecard must be valid (10 digits)
//Postcondition: Prints out the information of that patient to console
void seek(List& thePatients)
{
	string theCareCard = "";
	if (thePatients.getElementCount() == 0)
	{
		cout << "No Patient is registered Yet" << endl;
		
	}
	else {
	cout << "Please Enter the Patient CareCard Number: ";
	cin >> theCareCard;
	
	if (theCareCard.length() < 10) {
		cout << "Enter valid carecard number: ";
		cin >> theCareCard;
	}
	Patient temp_pt = Patient(theCareCard);
	Patient* to_print;

	if ((thePatients.search(temp_pt)) == NULL) {
		cout << "Can not find this Patient" << endl;
	}
	else {
		to_print = thePatients.search(temp_pt);
		cout << "\n" << endl;
		cout << "found the patient: " << endl;
		cout << *to_print;
	}
}
}

//Description: Allows the client to modify patients' personal information
//Precondition: There must be at least 1 patient in the list
//              The patient must exist in the list
//Postcondition: The patients' personal information is modified
void modify(List& thePatients)
{

	string theCareCard = "";

	if (thePatients.getElementCount() == 0)
	{
		cout << "No Patient is registered Yet" << endl;
	}

	cout << "Please Enter the Patient CareCard Number to Modify Patient Profile: ";
		cin >> theCareCard;
		cout << "\n";
	
	if (thePatients.search(theCareCard) != NULL)
	{
		Patient * p1 = thePatients.search(theCareCard);

		char Moinput;
		string MoName;
		string MoEmail;
		string MoAddress;
		string MoPhone;
		if (thePatients.search(theCareCard) == NULL)
		{
		cout << "This Patient is not registered" << endl;
		}


		else if (p1->getName() == "To be entered" && p1->getAddress() == "To be entered" && p1->getPhone() == "To be entered" && p1->getEmail() == "To be entered")
		{
			
			cout << "Please Enter the Name: ";
			getline(cin >> ws, MoName);
			p1->setName(MoName);
			


			cout << "Please Enter the Address: ";
			getline(cin >> ws, MoAddress);
			p1->setAddress(MoAddress);
			

			cout << "Please Enter the Phone Number: ";
			getline(cin >> ws, MoPhone);
			p1->setPhone(MoPhone);
			

			cout << "Please Enter the Email: ";
			getline(cin >> ws, MoEmail);
			p1->setEmail(MoEmail);
			


			cout << "\n"<<*thePatients.search(theCareCard);
		}
		else
		{
			bool done = false;
			while (not done)
			{
				cout << "To Change Name: n" << endl;
				cout << "To Change Address: a" << endl;
				cout << "To Change Email: e" << endl;
				cout << "To Change Phone: p\n" << endl;
				cout << "To Return to the Main Menu: x\n" << endl;
				cout << "Your Choice: ";
				cin >> Moinput;
				switch (Moinput)
				{
				case 'n':
					cout << "Please Enter the Name: ";
					getline(cin >> ws, MoName);
					p1->setName(MoName);
					cout << "\n";
					break;


				case 'a':
					cout << "Please Enter the Address: ";
					getline(cin >> ws, MoAddress);
					p1->setAddress(MoAddress);
					cout << "\n";
					break;


				case 'e':
					cout << "Please Enter the Email: ";
					getline(cin >> ws, MoEmail);
					p1->setEmail(MoEmail);
					cout << "\n";
					break;

				case 'p':
					cout << "Please Enter the Phone Number: ";
					getline(cin >> ws, MoPhone);
					p1->setPhone(MoPhone);
					cout << "\n";
					break;

				case 'x':
					cout << "\n Returning to the Main Menu\n" << endl;
					done = true;
					break;
				default:
					// operator is doesn't match any case constant (+, -, *, /)
					cout << "Error! operator is not correct\n";
				}
			}
		}
	}
}

//Description: Allows the client to remove all patients from the list
//Postcondition: All patients' information becomes unaccesible to the client
void removeAll(List& thePatients)
{
	char YNinput;
	cout << "Are you Sure? (y/n)";
	cin >> YNinput;
	YNinput = tolower(YNinput);
	switch (YNinput)
	{
	case 'y':
		cout << "All Patient Information has been deleted" << endl;
		thePatients.removeAll();
		break;

	case 'n':
		cout << "Patient Information has not been deleted" << endl; break;

	default:
		// operator is doesn't match any case constant (+, -, *, /)
		cout << "Error! operator is not correct";
		break;
	}

}

//Description: Prints the whole list
void print(List& thePatients)
{
	if (thePatients.getElementCount() == 0)
	{
		cout << "No Patient is registered Yet" << endl;
	}
	else
	{
		thePatients.printList();
	}
}


//Description: User interface
int main()
{
	List patients = List();
	bool done = false;
	char input = 0;
	while (not done)
	{

		cout << "\n----Welcome to Walk-in Clinic!----\n" << endl;
		cout << "To add a patient and create a profile\t\tenter: a" << endl;
		cout << "To remove a patient\t\t\t\tenter: r" << endl;
		cout << "To modify a patient profile\t\t\tenter: m" << endl;
		cout << "To search for a patient on the List\t\tenter: s" << endl;
		cout << "To print the patient list\t\t\tenter: p\n" << endl;

		cout << "To **Remove All** patient information\t\tenter: e" << endl;
		cout << "To exit the Patient Profile List\t\tenter: x\n" << endl;


		cout << "Your choice: ";
		cin >> input;
		input = tolower(input);
		switch (input) {
		case 'a': 
				add(patients); continue;
		case 'r': 
				remove(patients); continue;
		case 'e': 
				removeAll(patients); continue;
		case 'm': 
				modify(patients); continue;
		case 's': 
				seek(patients); continue;
		case 'p': 
				print(patients); continue;
		case 'x': 
				cout << "\n----Bye!\n" << endl; done = true; break;
		default: 
				cout << "Not sure what you mean! Please, try again!" << endl;

			cout << "There are now " << patients.getElementCount() << " Patients in List." << endl;
		}
		return 0;
	}
}