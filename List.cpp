/*
 * List.cpp
 *
 * Class Description: List data collection ADT.
 * Class Invariant: Data collection with the following characteristics:
 *                   - Each element is unique (no duplicates).
 *                   - When items are removed; they are overwritten (data is not deleted)
 *                   - Search function returns a pointer (memory address) of a patient class
 *                   
 *
 * Author: Michael Park(#301360758), Praneeth Ellanti (#301381164)
 * Date:Jan 22, 2019
 */
 
#include <iostream>
#include "List.h"

//constructor; sets elementCount to 0; and capacity to Max-elements
List::List() {
	elementCount = 0;
	capacity = MAX_ELEMENTS;
}

// Description: Returns the total element count currently stored in List.
int  List::getElementCount() const {
	return elementCount;
}


// Description: Insert an element and sorts the list on insert.
// Precondition: newElement must not already be in data collection, and the List is not full.  
// Postcondition: newElement inserted and elementCount has been incremented.   
bool List::insert(const Patient& newElement) {
	for (int j = 0; j < elementCount; j++)
	{
		if (elements[j] == newElement)
		{
			cout<<"This CareCard is already registered"<<endl;
			return false;
		}
	}

	if (elementCount >= capacity) {
		cout << "List is Full" << endl;
		return false;
	}
	

	int i;

	for (i = getElementCount() - 1; (i >= 0 && elements[i].getCareCard() > newElement.getCareCard()); i--) {
		elements[i + 1] = elements[i];
	}
	elements[i + 1] = newElement;
	elementCount++;
	return true;
}

// Description: Removes an element.
// Precondition: newElement must be present in data collection (otherwise returns false).  
// Postcondition: toBeRemoved is removed and elementCount has been decreased. 
bool List::remove(const Patient& toBeRemoved) {

	for (int i = 0; i < elementCount; i++)	{
		if (elements[i] == toBeRemoved) {
			for (int j = i; j < (capacity - 1); j++)
			{
				elements[j] = elements[j + 1];
			}
			elementCount--;
			return true;
		}
	}
	return false;
}

//Description: Sets the element count to 0 - (data is still present, but not accecible to client).
//Data will be overwritten when new Carecard is added
//Postcondition: Element count is set to 0; Previous data cannot be accessed 
void List::removeAll() {
	elementCount = 0;
}

//Description: Searches the list to see if target's carecard matches with the list.
//Precondition: Element must be in the list (else returns NULL)
//Postcondition: returns a pointer(memory address) to that patient from the list
Patient* List::search(const Patient& target) {
	for (int i = 0; i < getElementCount(); i++) {
		if (elements[i].getCareCard()==target.getCareCard())
		{
			return &elements[i];
		}
	}
	return NULL;
}

//Description: Prints the whole list
//Precondition: elements exist in the list
//Postcondition: outputs the list elements in console
void List::printList() {
	for (int i = 0; i < (this->getElementCount()); i++) {
		cout << elements[i];
	}
}